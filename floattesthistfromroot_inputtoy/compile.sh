#!/bin/bash

COMPILER=$(root-config --cxx)
FLAGS=$(root-config --cflags --libs)
echo $COMPILER $FLAGS

$COMPILER $FLAGS -L $ROOTSYS/lib -lRooFit -lRooFitCore -g -O3 -Wall -Wextra -Wpedantic -fopenmp ./doAll.cc ./testinputhist.C -I. -o doAll
