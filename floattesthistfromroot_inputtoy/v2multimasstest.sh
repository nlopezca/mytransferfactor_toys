#!/bin/bash

start=4
end=99

for ((i=start; i<=end; i++)); do
    ./doAll 0 100 toy $i
    ./doAll 0 125 toy $i
    ./doAll 0 175 toy $i
done

for ((i=0; i<=end; i++)); do
    ./doAll 0 150 toy $i
    ./doAll 0 200 toy $i
done
