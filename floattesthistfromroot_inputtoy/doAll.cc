#include <iostream>
#include "TString.h"
#include "testinputhist.h"
#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TROOT.h"
#include <string>

using namespace std ;

//void doAll(){
int main(int argc, char* argv[]){
    int n_inject = atoi(argv[1]);
  TString theLink  ;
  TString sigtheLink  ;
    TString mass(argv[2]);
    TString toyer(argv[3]);
    int n_tseed = atoi(argv[4]);

  theLink = "/home/nlopezca/testhistfromroot/QCDstorage/*.root";
  sigtheLink += "/home/nlopezca/testhistfromroot/SIGstorage/*"+mass+"*.root";

  cout << theLink << endl ;
  TChain * myChain = new TChain( "UFO" ) ;
  TTree * myTree ;

  myChain->Add( theLink );
  myChain->Add( sigtheLink );
  cout << "my chain = " << myChain->GetEntries() << endl ;
  
  gROOT->LoadMacro("/home/nlopezca/test_inputs/floattesthistfromroot_inputtoy/testinputhist.C+");
  testinputhist * myAnalysis ;
  myAnalysis =  new testinputhist( myChain ) ;
  myAnalysis->Loop(n_inject, mass,toyer,n_tseed );
  
  return 0;
}
