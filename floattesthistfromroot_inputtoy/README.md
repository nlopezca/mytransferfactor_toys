To run this code

source setup.sh

source compile.sh

./doAll n_inject mass toyer n_tseed

for n_inject, write the number of times you want to inject the signal, ex: 0, 1, 2, 3 ..

for mass, write the mass point you are searching for, 100, 125, 150, 175, 200

for toyer, delcare if you are making a toy, either toy or notoy

for n_tseed, (this only matter if toyer = toy), declare the seed being used for the random generation
