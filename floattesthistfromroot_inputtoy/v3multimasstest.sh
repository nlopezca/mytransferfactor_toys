#!/bin/bash

start=100
end=101

for ((i=start; i<=end; i++)); do
    ./doAll 0 100 toy $i 1> out"$i"_100.log 2> out"$i"_100.err &
    ./doAll 0 125 toy $i &
    ./doAll 0 175 toy $i &
done

for ((i=start; i<=end; i++)); do
    ./doAll 0 150 toy $i &
    ./doAll 0 200 toy $i &
done
