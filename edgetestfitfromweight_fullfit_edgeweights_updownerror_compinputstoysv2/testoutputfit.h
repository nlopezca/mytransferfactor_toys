//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Oct 30 14:21:57 2023 by ROOT version 6.28/08
// from TTree UFO/A tree
// found on file: QCD_MC16a_JZ6_test.root
//////////////////////////////////////////////////////////

#ifndef testoutputfit_h
#define testoutputfit_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class testoutputfit {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Float_t         UFO_DSID;
   Float_t         UFO_tau21_wta;
   Float_t         UFO_pt;
   Float_t         UFO_ANN_score;
   Float_t         UFO_m;
   Float_t         UFO_eta;
   Float_t         UFO_phi;
   Float_t         UFO_weight;
   Float_t         UFO_sumweights;

   // List of branches
   TBranch        *b_UFO_DSID;   //!
   TBranch        *b_UFO_tau21_wta;   //!
   TBranch        *b_UFO_pt;   //!
   TBranch        *b_UFO_ANN_score;   //!
   TBranch        *b_UFO_m;   //!
   TBranch        *b_UFO_eta;   //!
   TBranch        *b_UFO_phi;   //!
   TBranch        *b_UFO_weight;   //!
   TBranch        *b_UFO_sumweights;   //!

   testoutputfit(TTree *tree=0);
   virtual ~testoutputfit();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(int n_inject, TString mass, TString weights, TString toyer, TString tseed);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef testoutputfit_cxx
testoutputfit::testoutputfit(TTree *tree) : fChain(0)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("QCD_MC16a_JZ6_test.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("QCD_MC16a_JZ6_test.root");
      }
      f->GetObject("UFO",tree);

   }
   Init(tree);
}

testoutputfit::~testoutputfit()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t testoutputfit::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t testoutputfit::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void testoutputfit::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("UFO_DSID", &UFO_DSID, &b_UFO_DSID);
   fChain->SetBranchAddress("UFO_tau21_wta", &UFO_tau21_wta, &b_UFO_tau21_wta);
   fChain->SetBranchAddress("UFO_pt", &UFO_pt, &b_UFO_pt);
   fChain->SetBranchAddress("UFO_ANN_score", &UFO_ANN_score, &b_UFO_ANN_score);
   fChain->SetBranchAddress("UFO_m", &UFO_m, &b_UFO_m);
   fChain->SetBranchAddress("UFO_eta", &UFO_eta, &b_UFO_eta);
   fChain->SetBranchAddress("UFO_phi", &UFO_phi, &b_UFO_phi);
   fChain->SetBranchAddress("UFO_weight", &UFO_weight, &b_UFO_weight);
   fChain->SetBranchAddress("UFO_sumweights", &UFO_sumweights, &b_UFO_sumweights);
   Notify();
}

Bool_t testoutputfit::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void testoutputfit::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t testoutputfit::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef testoutputfit_cxx
