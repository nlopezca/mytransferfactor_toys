#include <iostream>
#include "TString.h"
#include "testoutputfit.h"
#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TROOT.h"
#include <string>

using namespace std ;

//void doAll(){
int main(int argc, char* argv[]){
    int n_inject = atoi(argv[1]);
  TString theLink  ;
  TString sigtheLink  ;
    TString mass(argv[2]);
    TString weights(argv[3]);
    TString toyer(argv[4]);
    TString tseed(argv[5]);

  theLink = "/home/nlopezca/testhistfromroot/QCDstorage/*.root";
  sigtheLink += "/home/nlopezca/testhistfromroot/SIGstorage/*"+mass+"*.root";

  cout << theLink << endl ;
  TChain * myChain = new TChain( "UFO" ) ;
  TTree * myTree ;

  myChain->Add( theLink );
  myChain->Add( sigtheLink );
  cout << "my chain = " << myChain->GetEntries() << endl ;
  
  gROOT->LoadMacro("/home/nlopezca/test_inputs/edgetestfitfromweight_fullfit_edgeweights_updownerror_compinputstoysv2/testoutputfit.C+");
  testoutputfit * myAnalysis ;
  myAnalysis =  new testoutputfit( myChain ) ;
  myAnalysis->Loop(n_inject, mass, weights,toyer,tseed);
  
  return 0;
}
