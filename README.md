Hello, this code for applying the transfer factor method works in 3 steps, running each step is described in detail in each folder
Step 1:
creating the intial histogram to input for the Transfer Factor method in floattesthistfromroot_inputtoy
Step 2:
creating the weights from the Transfer Factor method in testweightfromhist_parse_fine_condor
Step 3:
applying the weight for the extracing the final signal strength in edgetestfitfromweight_fullfit_edgeweights_updownerror_compinputstoysv2
